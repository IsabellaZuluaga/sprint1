require('dotenv').config();
const express = require ('express'); //Busca en node_modules el paquete express
const app = express (); //Express creame un servidor en la variable app

//Poner el puerto en una variable de enviroment
const PORT = process.env.PORT || 3000; //Se indica que use el puerto que he puesto en .env o de lo contrario (||) que use el 3000

app.use(express.json()); //ayuda a convertir el body a un json

app.get('/',(req, res) => { //Siempre va el req (peticion) y el response(respuesta que va a dar)
    res.send('Hola mundo');
});

const listaUsuarios = ['user1', 'user2', 'user3'];

//Obtener usuarios
app.get('/usuarios',(req,res)=> {//Con el /usuarios estoy indicando la ruta
    res.json(listaUsuarios); //res.json devuelve un json
});

//Crear usuarios
app.post('/usuarios', (req, res) => {
    const { nombre } = req.body; //Destructuring, es sacar la propiedad necesito
    listaUsuarios.push(req.body.nombre);
    res.json('Usuario creado exitosamente')
    //res.send('Post a ruta usuarios'); //res.send devuelve un texto plano
});

//Actualizar usuarios
app.put('usuarios', (req,res) => {
    const { index } = req.query;//nombre que recibe el parametro
    const { nombre } = req.body;
    listaUsuarios[index] = nombre;
    res.json('usuario actualizado');
});

//Borrar usuarios
app.delete('/usuarios', (req, res) => {
    const { index } = req.query; //Asi recibo el parametro
    listaUsuarios.splice(index, 1);
    res.json('Usuario eliminado')
});

app.listen(PORT, () => { //Debemos poner a escuchar al servidor, si funciona bien ejecuta la funcion flecha
    console.log('Escuchando desde el puerto ' + PORT);
});
