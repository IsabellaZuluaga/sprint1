const telefonos = [
    {
        marca: "Samsung",
        modelo: "S11",
        gama: "Alta",
        pantalla: "19:9",
        sistema_operativo: "Android",
        precio: 1200
    },
    {
        marca: "Iphone",
        modelo: "12 pro",
        gama: "Alta",
        pantalla: "OLED",
        sistema_operativo: "iOs",
        precio: 1500
    },
    {
        marca: "Xiomi",
        modelo: "Note 10s",
        gama: "Media",
        pantalla: "OLED",
        sistema_operativo: "Android",
        precio: 300
    },
    {
        marca: "LG",
        modelo: "LG el que sea",
        gama: "Alta",
        pantalla: "OLED",
        sistema_operativo: "Android",
        precio: 800
    }
];

//Crear metodos que obtengan la informacion
const obtenerTelefonos = () => {
    return telefonos;
}

const obtenerMitadTelefonos = () => {
    const mitad = Math.round(telefonos.length / 2);
    const mitadTelefonos = [];
    for (let index = 0; index < mitad; index++) {
        const telefono = telefonos [index];
        mitadTelefonos.push (telefono);
    }
    return mitadTelefonos;
}

//As se exportan los metodos, poniendolos entre {}
module.exports = { obtenerTelefonos, obtenerMitadTelefonos};