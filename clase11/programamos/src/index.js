const express = require('express');
const app = express();
const telefonos = require('./models/telefonos');


app.get('/telefonos', (req,res) => {
    res.json(telefonos.obtenerTelefonos());
});

app.get('/mitadTelefonos', (req,res) =>{
    res.json(telefonos.obtenerMitadTelefonos());
});


app.listen(3000, () => {
    console.log(('Escuchando en el puerto 3000'));
});