//con el fs.readdir le estoy indicando que lea los archivos
//cual? lo indico con el ./ para que lea el archivo en el que estoy
//Le indico que recorra la carpeta con el forEach y que imprima los file con el console

const fs = require('fs');

fs.readdir('./', (err, files) => {
    files.forEach(file => {
        console.log(file);
    });
})