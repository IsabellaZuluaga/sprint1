function hablar(texto) {
    console.log(texto);
}

function hablarBajo(texto) {
    console.log(texto.toLowerCase());
}
//Para poder exportar estas funciones y sean usadas en otros archivos
// 1. Exportando cada una
exports.hablar = hablar; 
exports.hablarBajo = hablarBajo; 

//2. Exportando todos los modulos 
//exports.modules = {hablarBajo, hablar}

//3. Poniendo export antes del nombre de la funcion