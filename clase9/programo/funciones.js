function holaClase() {
    return 'Hola Acamica';
}

console.log(holaClase());

const mensaje = (texto) => {
    console.log(`El texto enviado por parametro es: ${texto}`);
}

console.log(mensaje('Hola de nuevo Acamica'));