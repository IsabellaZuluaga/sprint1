//Crear array de hobbies
const hobbies = ['Leer', 'Jugar Futbol', 'Cantar', 'Correr', 'Ver peliculas'];
const fs = require ('fs');


//Mostrar cada hobbie con un forEach: Con esto, recorro todo el array y lo voy imprimiendo uno a uno en consola
hobbies.forEach((hobbie, indice) => {
    const texto = `${indice + 1}. ${hobbie}\n` //El \n indica un espacio hacia abajo
    console.log(texto);

    //Almacenar cada hobbie en un archivo txt con la libreria FS
    fs.appendFileSync('listaHobbies.txt', texto,function (err) { //Con el siync le indico que lo imprima en orden, con el append file crea el archivo txt
        if(err) console.log(err);
        else console.log('Saved!');
    });
});



