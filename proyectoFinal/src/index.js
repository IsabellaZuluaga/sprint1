require('dotenv').config();
const express = require('express');
const app = express();

const PORT = process.env.PORT || 3000;

app.use(express.json());

const usuariosRouter = require('./routes/usuarios.route');
const menuRouter = require('./routes/menu.router');


app.use('/usuarios',usuariosRouter);
app.use('/menu',menuRouter);


app.listen (PORT,() => {
    console.log('Escuchando en el puerto: ' + PORT);
});