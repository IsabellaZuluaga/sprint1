const express = require('express');
const router = express.Router();

const { obtenerUsuarios, crearUsuario } = require('../models/usuario.model');

//Falta validar que sea admin
router.get('/', (req,res) => {
    res.json(obtenerUsuarios());
});


router.post('/', (req,res) => {
    
    const { id, nombre, email, celular, direccion, username, password } = req.body;

    const usuarioNuevo = {
        'id': id,
        'nombre': nombre ,
        'email': email,
        'celular': celular,
        'direccion': direccion,
        'username': username,
        'password': password,
        'isAdmin':false
    };

    crearUsuario (usuarioNuevo);
    res.status(201).json('Usuario agregado'); 

    res.json('Se creo el usuario correctamente')
});




module.exports = router;