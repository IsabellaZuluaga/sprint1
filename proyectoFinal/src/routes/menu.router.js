const express = require('express');
const router = express.Router();
const { obtenerProductos,crearProducto, eliminarProducto } = require('../models/producto.model');


router.get('/', (req,res) =>{
    res.json(obtenerProductos());
});


//Falta crear el middleware de que esta accion solo lo puede hacer el admin
router.post('/', (req,res) =>{
    const {id, nombre, precio} = req.body;
    
    const nuevoProducto = {
            'id': id,
            'nombre' : nombre,
            'precio' : precio
        };

    crearProducto(nuevoProducto);
    res.status(201).json('Producto creado exitosamente');
});


router.delete('/:id',(req,res) => {
    const { id } = req.body;

    res.json('Producto Eliminado exitosamente');


});



module.exports = router;