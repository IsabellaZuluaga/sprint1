//Creo el array de usuarios con los objetos que incluye ya la existencia de usuarios, con sus propiedades. 
const usuarios = [
    {
        'id': 1,
        'nombre': 'Isabella',
        'email': 'isazuluocampo@gmail.com',
        'celular': '604137164',
        'direccion': 'Calle 12 # 48a-60',
        'username': 'isabellazuluaga',
        'password': '12345',
        'isAdmin':true
    },
    {
        'id': 2,
        'nombre': 'Gloria',
        'email': 'gloriaocampo@gmail.com',
        'celular': '3136419323',
        'direccion': 'Calle 12 # 49-60',
        'username': 'gloriaocampo',
        'password': '123',
        'isAdmin':false
    }
];

//A partir de aqui creo las acciones (funciones- metodos) que tiene este usuario (interactua con si mismo).
//Obtiene la lista de todos los usuarios creados con sus propiedades
const obtenerUsuarios = () => {
    return(usuarios)
};

//Crea un nuevo usuario, por lo cual debe solicitarse por body que se ingrese esta informacion (Esta solicitud se hace por la ruta especifica usuarios). 
const crearUsuario = (usuarioNuevo) => {
    usuarios.push(usuarioNuevo)
};

//Siempre debo exportar las funciones que cree para ser usadas en otra parte del codigo. 
module.exports = { obtenerUsuarios, crearUsuario };
