const productos = [
    {
        'id': 1,
        'Nombre': 'Pasta',
        'Precio': '16.000'
    },
    {
        'id': 2,
        'Nombre': 'Mariscos',
        'Precio': '25.000'
    },
    {
        
        'id': 3,
        'Nombre': 'Paella',
        'Precio': '22.000',
    },
    {
        'id': 4,
        'Nombre': 'Bocadillo',
        'Precio': '10.000',
    }
];

const obtenerProductos = () => {
    return(productos);
};

const crearProducto = (productoNuevo) => {
    productos.push(productoNuevo);
};

const obtenerProductoId = (id) => {
    return productos.find(producto => producto.id == id);
}

const eliminarProducto = (productoEliminado) => {
    productos.splice(productoEliminado);
};
    


module.exports = {obtenerProductos, crearProducto, eliminarProducto}; 