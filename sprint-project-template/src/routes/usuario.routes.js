const express = require('express');
const router = express.Router(); //llamo la ruta de route

const { agregarUsuario, obtenerUsuarios } = require('../models/usuario.model');

/**
 * @swagger
 *  /usuarios:
 *      get:
 *          summary: Obtener todos los usuarios del sistema *           
 *          tags: [Usuarios]
 *          responses:
 *              200:
 *                  description: Lista de usuarios del sistema
 *                  content:
 *                      application/json:
 *                          schema:
 *                              type: array
 *                              items:
 *                              $ref: '#/components/schemas/usuario' 
 */


//Ya no seria app sino router, porque la ruta es router
router.get('/', (req, res) => { //si estoy creando usuarios, debo tener un array que reciba esta informacion
    console.log(req.auth.user);
    res.json(obtenerUsuarios()); //Aqui esta respondiendo la lista de usuarios en el servidor
})

/**
 * @swagger
 * /usuarios:
 *  post:
 *      summary: Crea un usuario en el sistema           
 *      tags: [Usuarios]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/usuario'
 *      responses:
 *          201:
 *              description: Usuario creado
 *          401:
 *              description: Usuario y contrasena incorrectos     
 */

router.post('/', (req, res) => { //si estoy creando usuarios, debo tener un array que reciba esta informacion
    res.sendStatus(201); //Aqui esta respondiendo la lista de usuarios en el servidor
})


router.put('/:email', (req, res) => {
    console.log(req.body);
    res.json(usuario);
});

router.delete('/:email', (req, res) => {
    res.json('Usuario eliminado');
});

/**
 * @swagger
 * tags:
 *  name: Usuarios
 *  description: Seccion de usuarios
 *  
 * components:
 *  schemas:
 *      usuario:
 *          type: object
 *          required:
 *              email
 *              contrasena
 *          properties:
 *              email:
 *                  type: string
 *                  description: Email del usuario
 *              contrasena:
 *                  type: string
 *                  description: Contrasena del usuario
 *          examples:
 *              email: usuario@gmail.com
 *              contrasena: 1234abc 
 */
module.exports = router;