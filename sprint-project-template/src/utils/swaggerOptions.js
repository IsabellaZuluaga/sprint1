const swaggerOptions = {//debo abrir un json para poner las especificaciones
    definition: {//Siempre empieza con este definition
        openapi: "3.0.0",//siempre es esta version
        info: {
            title: "Sprint project 1 Protalento",
            version: "0.0.1",
            description: "Proyecto 1 para acamica DWBE",
        },
        servers: [
            {
                url: "http://localhost:3000",
                description: "Local server"
            }
        ],
        components:{
            securitySchemes: {
                basicAuth: {
                    type: "http",
                    scheme: "basic"
                }
            }
        },
        security:[
            {
                basicAuth: []
            }
        ] 
    },
        apis: ["./src/routes/*.js"]//A partir de esta carpeta encuentra todos los archivos js, porque aqui tengo todas las rutas (endpoints)
};

module.exports = swaggerOptions;