const express = require('express');
const basicAuth = require('express-basic-auth'); //Requerimos la libreria express basic auth
const swaggerJsDoc = require('swagger-jsdoc'); //importo las librerias de swagger
const swaggerUI = require('swagger-ui-express');

const usuarioRoutes = require('./routes/usuario.routes');

const autenticacion = require('./middlewares/autenticacion.middleware');
const swaggerOptions = require('./utils/swaggerOptions');

const app = express();
app.use(express.json()); //Necesario para recibir en el request.body


//uso las librerias de swagger, esto es un estandar 
const swaggerSpecs = swaggerJsDoc(swaggerOptions);

//Para ver la documentacion de swagger
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpecs));//Cuando alguien entre a esa url, lo atiende swaggerUI y saca la documentacion de la especificacion que cree.

//creamos el middleware para que use el basicAuth
app.use(basicAuth({ authorizer: autenticacion }));

app.use('/usuarios', usuarioRoutes ); //Toda peticion que hagan a usuarios lo va a atender usuarioRoutes


app.listen(3000, () => {
    console.log('Escuchando en el puerto 3000');
});