const usuarios = [
    {
        username: "Isabella",
        password: "12345",
        isAdmin: true
    },
    {
        username: "admin",
        password: "12345",
        isAdmin: true
    }
];

//Creo los metodos o acciones que interactua con el array, es decir con usuarios
const obtenerUsuarios = () => {
    return usuarios;
}

const agregarUsuario = (usuarioNuevo) => {
    usuarios.push(usuarioNuevo);
}

//Exporto los metodos
module.exports = { obtenerUsuarios, agregarUsuario }
