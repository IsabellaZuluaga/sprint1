//Instalar y requerir la libreria moment
var moment = require('moment');

//Mostrar horario local
console.log( "Horario local: " + moment().format()); //Imprime la fecha

//Mostrar horario UTC
console.log("Horario UTC: " + moment.utc().format());

//Mostrar diferencia horaria
const diferencia = moment().hour() - moment.utc().hour();
console.log(`${moment.utc().hour()} - ${moment().hour()} = ${diferencia - 24}`);


//Comparar dos fechas y decir cual es mayor
const fechaInicial = moment('2060-01-01');

if(fechaInicial.isBefore('2050-01-01')) {
    console.log('La fecha inicial es anterior al ano 2050');
}else {
    console.log('La fecha inicial es posterior al ano 2050');
}


//INSTALAR NODEMON: npm install -g nodemon, esto me sirve para que el programa se vaya ejecutando en consola en cuanto yo guarde una modificacion
//Para parar es Ctrl C