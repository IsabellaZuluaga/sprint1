require('dotenv').config(); //Por favor trae del node_modules el archivo .env y dale configurar
const env = require('./appsetings.json'); //Con esta variable env me traigo todo el json que tengo en appsettings

const node_env = process.env.NODE_ENV || "Development"; //Con esto me traigo la informacion del archivo .env y en caso de que no exista le digo que se vaya para el ambiente Development
const variables = env[node_env]; //En esta extraigo solo la informacion que guarde en cada ambiente del archivo appsettings

//Este const variables = env[] se escribe con los corchetes porque es una propiedad dinamica, entonces depende del valor que esta tenga, va a cambiar su informacion. 
//En este caso tambien podria escribir env.QA para que me traiga solo la informacion de QA

console.log(node_env); //Imprimo la informacion del archivo .env
console.log(variables);//Imprimo la informacion del appsetings solo del ambiente en el que estoy

