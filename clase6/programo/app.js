//Aqui defino la funcion
function funcionPrueba(){
    console.log('Hola desde una funcion');
}

funcionPrueba(); //Aqui llamo la funcion, esto es fundamental para que se ejecute la funcion

//Funcion con un parametro
function funcionPruebaDos(nombre){
    console.log('Hola desde una funcion ' + ' ' + nombre);
}

funcionPruebaDos("Mauricio");
funcionPruebaDos("Ronaldo");
funcionPruebaDos("Isabella");

//Funcion con dos parametros
function funcionPruebaTres(nombre, apellido){
    console.log('Hola desde una funcion ' + ' ' + nombre + ' ' + apellido);
}

funcionPruebaTres("Mauricio", "Sierra");
funcionPruebaTres("Ronaldo", "Cristiano");
funcionPruebaTres("Isabella", "Zuluaga");

//VARIABLES-Se recomienda no usar VAR, sino CONST y para variables cambiantes LET
let texto2 = "Hola LET JS";
texto2 = "Hola LET JS!!" //Aqui estoy redifiendo el valor de texto 2

const texto3 = "HOla desde constante" //Una variable constante que no se puede modificar

if(true){
    let textoIf = "Hola desde dentro del if";
    console.log(textoIf);
    console.log(texto2);
}

function dentroDeFuncion() {
    var texto4 = "Hola texto 4"
}

//Funciones flecha:
function saludar() {
    console.log('Hola');
}

//Lo mismo de arriba pero se llama funcion anonima
let saludar2 = function () { //Esto es lo mismo que arriba
    console.log('Hola2');
}

saludar(); //Aqui las estoy llamando
saludar2();

//Funciones arrows lo mismo de arriba pero usando las funciones flecha

let saludar3 = () => {
    console.log('Hola3');
}

saludar3(); 

document.getElementById('btn').addEventListener('click', saludar2);

function suma(n1, n2) {
    console.log(n1 + n2);
}

let suma2 = function (n1, n2) {
    console.log(n1 + n2);
}

let suma3 = (n1, n2) => {
    console.log(n1 + n2);
}

//Para simplicar lo de arriba puedo hacerlo de la siguiente manera (Esto solo aplica cuando lo que esta dentro del {} es solo una linea)
let suma4 = (n1, n2) => console.log(n1 + n2);

suma(1, 2);
suma2(2, 3);
suma3(3, 4);
suma4(1, 2);

let elDoble = (n1) => {
    return n1 * 2;
}

console.log(elDoble(5));

let elDoble2 = n1 => n1 * 2; //Esto es lo mismo de arriba pero solo aplica porque tiene un solo parametro y solo una linea

console.log(elDoble2(2));