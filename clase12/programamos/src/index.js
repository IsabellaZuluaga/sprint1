const express = require('express');
const app = express();

app.use(express.json());

const usuarios = [
    { id: 1, nombre: 'Pepe', email: 'pepe@nada.com' },
    { id: 2, nombre: 'Hugo', email: 'hugo@nada.com' },
    { id: 3, nombre: 'Juan', email: 'juan@nada.com' }
];

//Middleware global que devuelve la ruta que esta ejecutando
app.use((req, res, next) => {
    console.log(req.url);
    next();
});

//EndPoint que devuelve el usuario buscado por id
app.get('/usuarioporid/:id', (req,res) => {//esto es un query params, cuando requiero una informacion a partir de un parametro
    const id = req.params.id;
    const filtro = usuarios.find(u => u.id == id);
    if (filtro) {//si existe algo en la posicion 0 de usuario
        res.json(filtro)
    }else {
        res.status(404).json('Usuario no encontrado');
    }
});

//EndPoint que devuelve el usuario buscado por nombre
app.get('/usuariopornombre/:nombre', (req,res) => {//esto es un query params, cuando requiero una informacion a partir de un parametro
    const nombre = req.params.nombre;
    const filtro = usuarios.find(u => u.nombre == nombre);
    if (filtro) {//si existe algo en la posicion 0 de usuario
        res.json(filtro)
    }else {
        res.status(404).json('Usuario no encontrado');
    }
});





app.listen(3000);

