const express = require('express');
const app = express (); //Crea el servidor de express
const { imprimirRequest, imprimirRequest2, imprimirRequest3 } = require('./middlewares/imprimir.middleware') //Importo los middleware con un destructurin

app.use(imprimirRequest); //Asi ejecuta primero el middleware antes del ejemplo

app.get('/ejemplo', (req,res) => {
    res.json('Respuesta ejemplo desde endPoint') //Responde al servidor
    console.log('Respuesta ejemplo desde endPoint terminal'); //Responde a la terminal
});

app.get('/ejemplo2',imprimirRequest2, imprimirRequest3, (req,res) => {
    res.json('Respuesta ejemplo2 desde endPoint')
    console.log('Respuesta ejemplo2 desde endPoint terminal');
}); 

app.listen(3000, () => {
    console.log('Escuchando desde el puerto 3000');
});