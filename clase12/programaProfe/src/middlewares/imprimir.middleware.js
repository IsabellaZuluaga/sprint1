function imprimirRequest(req, res, next){
    console.log("Hora desde Middleware: ", Date.now());
    next();//ejecute lo que venga despues
}

function imprimirRequest2(req, res, next){
    console.log("Path desde middleware2: ", req.path);
    //res.json('No estas autorizado'); //Con eeste res.json estoy impidiendo que pase a la ruta, porque le puse next, solo con el next pasa. 
    if(req.path === '/ejemplo2'){ //Si la ruta que esta llamando es ejemplo 2 siga, sino diga que no esta autorizado.
        next();
    }else {
        res.json('No autorizado');
    }
    
}

function imprimirRequest3(req, res, next){
    console.log("Path desde middleware3: ", req.path);
    res.status(401).json('No estas autorizado'); //Con este res.json estoy impidiendo que pase a la ruta, porque le puse next, solo con el next pasa. 
}

module.exports = { imprimirRequest, imprimirRequest2, imprimirRequest3 }