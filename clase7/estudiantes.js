class estudiante {
    constructor(nombre, apellido, programa){
        this.nombre = nombre;
        this.apellido = apellido;
        this.programa = programa;
    }

    hablar (){
        console.log(`Hola, soy ${this.nombre}, un gusto conocerte`);
    }

    caminar (){
        console.log(`Tiempo de moverse, ellos te estan esperando`);
    }

    participar (){
        console.log(`Sobre este documento, considero que deberiamos incluir esto`);
    }
}

const estudiante1 = new estudiante("Isabella", "Zuluaga", "programadora");

estudiante1.hablar();