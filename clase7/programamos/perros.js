class Perro {
    constructor(nombre, raza, edad, color){
        this.nombre = nombre;
        this.raza = raza;
        this.edad = edad;
        this.color = color;
        this.estadoAdopcion = "En adopcion"; //Aqui estoy incluyendo una propiedad sin incluirla en el constructo porque ya tiene un valor asignado
    }

    ObtenerEstadoAdopcion (){
        return this.estadoAdopcion;
    }
    establecerEstadoAdopcion (estado){
        const estados = ['En adopcion', 'Proceso de adopcion', 'Adoptado']   
        this.estadoAdopcion = estados [estado - 1];
    }
}

function imprimirListas(lista, mensaje) {
    
    for (const perro of lista){
        mensaje += `n${perro.nombre}`;
    }
    console.log(mensaje);
}
let validacion = true;
const perros = [];

while (validacion){
    nombre = prompt('Ingrese el nombre del perro'),
    raza = prompt('Ingrese la raza del perro'),
    edad = prompt('Ingrese la edad del perro '),
    color = prompt('Ingrese el color del perro ');
    estadoAdopcion = parseInt(prompt('Estado de aopcion (1: En adopcion, 2: Proceso de adopcion, 3: Adoptado'));
}

let nuevoPerro = new Perro(nombre, raza, edad, color);
nuevoPerro. establecerEstadoAdopcion(estadoAdopcion);
perros.push(nuevoPerro);

validacion = window.confirm('Quiere continuar?');

imprimirListas(perros, 'Todos los perros:');
imprimirListas(perros.filter(perro => perro.ObtenerEstadoAdopcion() === 'En adopcion'), 'Perros en adopcion');
imprimirListas(perros.filter(perro => perro.ObtenerEstadoAdopcion() === 'Proceso de adopcion'), 'Perros en proceso de adopcion');
imprimirListas(perros.filter(perro => perro.ObtenerEstadoAdopcion() === 'Adoptado'), 'Perros adoptados');